package com.tekup.carpool_backend.model.user;

public enum UserRole {
    ADMIN,
    DRIVER,
    PASSENGER
}
